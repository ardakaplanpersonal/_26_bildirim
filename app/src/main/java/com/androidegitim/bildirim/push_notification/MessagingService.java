package com.androidegitim.bildirim.push_notification;


import android.content.Intent;

import com.androidegitim.androidegitimlibrary.helpers.NotificationHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.bildirim.MainActivity;
import com.androidegitim.bildirim.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by ardakaplan on 18/10/16.
 * <p/>
 * www.ardakaplan.com
 * <p/>
 * arda.kaplan09@gmail.com
 */
public class MessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        String value = "";

        Map<String, String> data = remoteMessage.getData();

        for (Map.Entry<String, String> entry : data.entrySet()) {

            RDALogger.info(entry.getKey() + " : " + entry.getValue());

            value = value + " " + entry.getValue();
        }


//        RDALogger.info(remoteMessage.getNotification().getTitle());
//
//        RDALogger.info(remoteMessage.getNotification().getBody());


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        intent.putExtra("DATA", value);

        NotificationHelpers.showNotification(getApplicationContext(), getString(R.string.app_name),
                                             remoteMessage.getNotification().getBody(),
                                             intent,
                                             R.drawable.icon_notification,
                                             true,
                                             12,
                                             true);


    }
}