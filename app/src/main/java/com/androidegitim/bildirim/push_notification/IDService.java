package com.androidegitim.bildirim.push_notification;


import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by ardakaplan on 18/10/16.
 * <p/>
 * www.ardakaplan.com
 * <p/>
 * arda.kaplan09@gmail.com
 */
public class IDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        // Get updated InstanceID token.

        RDALogger.info("Refreshed token: " + getToken());
    }

    public static String getToken() {

        return FirebaseInstanceId.getInstance().getToken();
    }
}