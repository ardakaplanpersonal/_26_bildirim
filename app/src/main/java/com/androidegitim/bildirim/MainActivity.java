package com.androidegitim.bildirim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.androidegitim.androidegitimlibrary.helpers.NotificationHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.bildirim.push_notification.IDService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


//    https://firebase.google.com/

    @BindView(R.id.main_textview_coming_date)
    TextView dataTextView;

    private int notificationId = 0;

    private int nonDeletableNotificationID = 120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        ButterKnife.bind(this);

        RDALogger.info(IDService.getToken());


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("DATA")) {

            dataTextView.setText(getIntent().getExtras().getString("DATA"));

        } else {

            dataTextView.setText("DATA YOK");
        }

    }

    @OnClick(R.id.main_button_create_notification)
    public void createNotification() {

        NotificationHelpers.showNotification(getApplicationContext(), getString(R.string.app_name),
                                             "Silinebilen bildirim",
                                             new Intent(MainActivity.this, MainActivity.class),
                                             R.drawable.icon_notification,
                                             true,
                                             notificationId,
                                             true);

        notificationId++;
    }

    @OnClick(R.id.main_button_create_ongoing_notification)
    public void createOngoingNotification() {

        NotificationHelpers.showNotification(getApplicationContext(), getString(R.string.app_name),
                                             "Silinemeyen bildirim",
                                             new Intent(MainActivity.this, MainActivity.class),
                                             R.drawable.icon_notification,
                                             true,
                                             nonDeletableNotificationID,
                                             false);
    }


    @OnClick(R.id.main_button_cancel_notification)
    public void cancelNotification() {

        NotificationHelpers.cancelNotification(getApplicationContext(), nonDeletableNotificationID);
    }
}
